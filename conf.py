import os


DOMAIN = "http://localhost:8080"
API_PATH = "/api/v0"
GOD_USER = {"username": "root", "password": "123"}
PROJECT_DIR_PATH = os.getcwd()
YAML_FILENAME = "schedule.yaml"
YAML_FILE_PATH = os.path.join(PROJECT_DIR_PATH, YAML_FILENAME)
LOG_FILENAME = "fill_schedule.log"
LOG_FILE_PATH = os.path.join(PROJECT_DIR_PATH, LOG_FILENAME)
