# SRE Tinkoff KFU 2023 Kazymov O.

Репозиторий в который я буду выкладывать домашние задания по курсу SRE от Тинькофф

## HW 1.1 Oncall fix
### Задание
При первоначальном запуске приложения, на этапе наполнения базы данных данными происходит ошибка:
```
~$ docker-compose up
...
DB successfully loaded /home/oncall/db/schema.v0.sql
Importing /home/oncall/db/dummy_data.sql...
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1136 (21S01) at line 16: Column count doesn't match value count at row 1
Ran into problems during DB bootstrap. oncall will likely not function correctly. mysql exit code: 1 for /home/oncall/db/dummy_data.sql
Failed to load dummy data.Wrote /home/oncall/db_initialized so we don't bootstrap db again
...
```
Моей задачей было разобраться с этой ошибкой.
### Путь к нахождению решения
Как понятно из сообщения об ошибке, количество столбцов в таблице `team` не совпадает с количеством значений, которые скрипт пытается вписать в нее. Проверив содержимое файлов из папки `db`, я обнаружил, что в новой версии приложения в таблице `team` появился столбец `api_managed_roster`. Я нашел 2 решения этой проблемы: первый - это просто добавить в строчку с запросом на вставку одно значение `DEFAULT` в конец, и второе - явно перечислить названия столбцов, в которые скрипт записывает данные. Первый вариант проще, но менее надежен, т.к. возможно, что в будущем к этой таблице добавится еще один столбец, и тогда снова возникнет ошибка. Поэтому я решил явно перечислить имена столбцов.

Точнее, я изменил 16-ую строку в файле `./oncall/db/dummy_data.sql` c
```
"INSERT INTO `team` VALUES (1,'Test Team','#team','#team-alerts','team@example.com','US/Pacific',1,NULL,0,NULL);"
```
на
```
INSERT INTO `team`(`id`, `name`, `slack_channel`, `slack_channel_notifications`, `email`, `scheduling_timezone`, `active`, `iris_plan`, `iris_enabled`, `override_phone_number`) VALUES (1,'Test Team','#team','#team-alerts','team@example.com','US/Pacific',1,NULL,0,NULL);
```
После внесенных изменений докер поднялся без ошибок и данные отобразились в веб интерфейсе oncall.

### Порядок выполнения необходимых действий
предполагается, что рабочей директорией является папка `oncall`
- остановить докер, удалить контейнеры и вольюмы, т.к. некоторые данные уже создались и при повторном запуске возникнет ошибка из-за дублирования информации. нужно исполнить команду `docker-compose down --remove-orphans --volumes`
- отредактировать файл `./oncall/db/dummy_data.sql`, изменив 16-ую строку, как было показано ранее
- поднять докер, повторно сбилдив образы командой `docker-compose up --build`
- проверить отображается ли веб-страница

## HW 1.2 OnCall events
### Задание
В департаменте инфраструктуры несколько команд SRE, мне как SRE предстоит задача по автоматизации процесса дежурств, а именно - разработка приложения, которое по REST API создаст команды/сотрудников команд и их дежурства согласно следующему описанию:
```
---
teams:
  - name: "k8s SRE"
    scheduling_timezone: "Europe/Moscow"
    email: "k8s@sre-course.ru"
    slack_channel: "#k8s-team"
    users:
      - name: "o.ivanov"
        full_name: "Oleg Ivanov"
        phone_number: "+1 111-111-1111"
        email: "o.ivanov@sre-course.ru"
        duty:
          - date: "02/10/2023"
            role: "primary"
          - date: "03/10/2023"
            role: "secondary"
          - date: "04/10/2023"
            role: "primary"
          - date: "05/10/2023"
            role: "secondary"
          - date: "06/10/2023"
            role: "primary"
      - name: "d.petrov"
        full_name: "Dmitriy Petrov"
        phone_number: "+1 211-111-1111"
        email: "d.petrov@sre-course.ru"
        duty:
          - date: "02/10/2023"
            role: "secondary"
          - date: "03/10/2023"
            role: "primary"
          - date: "04/10/2023"
            role: "secondary"
          - date: "05/10/2023"
            role: "primary"
          - date: "06/10/2023"
            role: "secondary"

  - name: "DBA SRE"
    scheduling_timezone: "Asia/Novosibirsk"
    email: "dba@sre-course.ru"
    slack_channel: "#dba-team"
    users:
      - name: "a.seledkov"
        full_name: "Alexander Seledkov"
        phone_number: "+1 311-111-1111"
        email: "a.seledkov@sre-course.ru"
        duty:
          - date: "02/10/2023"
            role: "primary"
          - date: "03/10/2023"
            role: "primary"
          - date: "04/10/2023"
            role: "primary"
          - date: "05/10/2023"
            role: "secondary"
          - date: "06/10/2023"
            role: "primary"
      - name: "d.hludeev"
        full_name: "Dmitriy Hludeev"
        phone_number: "+1 411-111-1111"
        email: "user-4@sre-course.ru"
        duty:
          - date: "02/10/2023"
            role: "secondary"
          - date: "03/10/2023"
            role: "secondary"
          - date: "04/10/2023"
            role: "vacation"
          - date: "05/10/2023"
            role: "primary"
          - date: "06/10/2023"
            role: "secondary"
```
### Решение
Я написал скрипт на питоне, автоматически создающий команды, сотрудников и дежурства согласно описанию в yaml-файле. Исходный код находится в файле `fill_schedule.py`. Настройка конфигурационных переменных производится путем изменения их значений в файле `conf.py`.

[Скринкаст](https://youtu.be/X2z1DiVBmAk)
### Порядок выполнения необходимых действий
предполагается, что вы находитесь в корневой папке проекта и у вас установлен интерпретатор Python 3.11
- установка зависимостей `python -m pip install -r requirments.txt`
- настройка конфигурационных переменных в файле `conf.py`
- запуск сайта oncall `cd oncall; docker-compose up -d`
- запуск скрипта `cd ..; python fill_schedule.py`
- проверка логов `cat fill_schedule.log`
- проверка создания команд, сотрудников и дежурств в веб-интерфейсе
- выключение сайта `cd oncall; docker-compose down`
