import os
import time
import docker
import json

client = docker.from_env()

ACCESS_LOG_PATH = os.getenv("ONCALL_ACCESS_LOG_PATH")
TEXTFILES_DIR = os.getenv("ONCALL_TEXTFILES_DIR")


def main():
    # try:
    #     with open(ACCESS_LOG_PATH, "r") as access_log_file:
    #         lines = access_log_file.readlines()
    # except IOError:
    #     lines = []

    api_requests_num = {
        200: 0,
        201: 0,
        404: 0,
        500: 0,
        503: 0
    }
    # for line in lines:
    #     if " /api/v0/" in line:
    #         for code in api_requests_num.keys():
    #             if f"[{code}]" in line:
    #                 api_requests_num[code] += 1

    for log_line in client.containers.get('oncall-oncall-web-1').logs(stream=True, follow=True):
        log_line = log_line.decode('utf-8')
        if not log_line.startswith('{'):
            continue
        msg = json.loads(log_line)
        if msg["source"] == "uwsgi-req":
            for code in api_requests_num.keys():
                if f"[{code}]" in msg["msg"]:
                    api_requests_num[code] += 1

            metric_text = \
                '''# HELP api_requests_total Total number of request to oncall api by HTTP status code.
            # TYPE api_requests_total counter
            api_requests_total{code="200"} %d
            api_requests_total{code="201"} %d
            api_requests_total{code="404"} %d
            api_requests_total{code="500"} %d
            api_requests_total{code="503"} %d
            ''' % (
                    api_requests_num[200],
                    api_requests_num[201],
                    api_requests_num[404],
                    api_requests_num[500],
                    api_requests_num[503]
                )
            with open(os.path.join(TEXTFILES_DIR, "apireqs.prom"), "w") as prom_file:
                prom_file.write(metric_text)


if __name__ == '__main__':
    main()
        # time.sleep(5)
