from environs import Env

env = Env()
env.read_env()


class SLACounterConfig:
    PROMETHEUS_API_URL = env('PROMETHEUS_API_URL', 'http://localhost:9090')
    LOG_LEVEL = env('SLA_COUNTER_LOG_LEVEL')
    INTERVAL = int(env('METRICS_INTERVAL'))
    MYSQL_HOST = env('MYSQL_HOST', 'localhost')
    MYSQL_PORT = env.int('MYSQL_PORT', 3306)
    MYSQL_USER = env('MYSQL_USER', 'root')
    MYSQL_PASS = env('MYSQL_PASS', '1234')
    MYSQL_DB_NAME = env('MYSQL_DB_NAME', 'sla')
