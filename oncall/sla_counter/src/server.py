import logging
import sys
import time
from datetime import datetime

import requests
from mysql import connector

from conf import SLACounterConfig


class Mysql:
    def __init__(self, config: SLACounterConfig) -> None:
        logging.info('Connecting db')
        self.connection = connector.connect(
            host=config.MYSQL_HOST, user=config.MYSQL_USER,
            passwd=config.MYSQL_PASS, auth_plugin='mysql_native_password'
        )
        self.table_name = 'indicators'

        logging.info('Starting migration')

        cursor = self.connection.cursor()
        cursor.execute('CREATE DATABASE IF NOT EXISTS %s' % (config.MYSQL_DB_NAME))

        cursor.execute('USE sla')

        cursor.execute("""
            CREATE TABLE IF NOT EXISTS %s(
            datetime datetime not null default NOW(),
            name varchar(255) not null,
            slo float (4) not null,
            value float (4) not null,
            is_bad bool not null default false)
            """ % (self.table_name))

        cursor.execute("""
            ALTER TABLE %s ADD INDEX (datetime)
        """ % (self.table_name))

        cursor.execute("""
            ALTER TABLE %s ADD INDEX (name)
        """ % (self.table_name))

    def save_indicator(self, name, slo, value, is_bad=False, time=None):
        cursor = self.connection.cursor()
        sql = f"INSERT INTO {self.table_name} (name, slo, value, is_bad, datetime) VALUES (%s, %s, %s, %s, %s)"
        val = (name, slo, value, int(is_bad), time)
        cursor.execute(sql, val)
        self.connection.commit()


class PrometheusRequest:
    def __init__(self, config: SLACounterConfig):
        self.prometheus_api_url = config.PROMETHEUS_API_URL

    def last_value(self, query, time, default):
        try:
            response = requests.get(
                self.prometheus_api_url + '/api/v1/query',
                params={'query': query, 'time': time}
            )
            content = response.json()
            if not content:
                return default

            if len(content['data']['result']) == 0:
                return default

            return content['data']['result'][0]['value'][1]
        except Exception as e:
            logging.error(e)
            return default


def setup_logging(config: SLACounterConfig):
    logging.basicConfig(
        stream=sys.stdout,
        level=config.LOG_LEVEL,
        format='%(asctime)s %(levelname)s:%(message)s'
    )


def main():
    config = SLACounterConfig()
    setup_logging(config)
    db = Mysql(config)
    prom = PrometheusRequest(config)
    logging.info(f"Starting sla counter")
    while True:
        logging.info(f"Run prober")
        unixtimestamp = int(time.time())
        date_format = datetime.utcfromtimestamp(unixtimestamp).strftime('%Y-%m-%d %H:%M:%S')
        is_this_minute_bad = False
        value = prom.last_value(
            'increase(custom_prober_create_roster_success_total[1m])', unixtimestamp, 0
        )
        value = int(float(value))
        is_this_minute_bad = is_this_minute_bad or value < 1
        db.save_indicator(
            name='custom_prober_create_roster_success_total', slo=1, value=value, is_bad=value < 1, time=date_format
        )
        value = prom.last_value(
            'increase(custom_prober_create_roster_fail_total[1m])', unixtimestamp, 0
        )
        value = int(float(value))
        is_this_minute_bad = is_this_minute_bad or value > 0
        db.save_indicator(
            name='custom_prober_create_roster_fail_total', slo=0, value=value, is_bad=value > 0, time=date_format
        )
        value = prom.last_value(
            'custom_prober_create_roster_duration_seconds', unixtimestamp, 2)
        value = float(value)
        is_this_minute_bad = is_this_minute_bad or value > 0.2
        db.save_indicator(
            name='custom_prober_create_roster_duration_seconds', slo=0.2, value=value, is_bad=value > 0.2, time=date_format
        )
        db.save_indicator(
            name='sla_is_met', slo=0, value=1 if is_this_minute_bad else 0, is_bad=is_this_minute_bad, time=date_format
        )
        logging.info(f"Waiting {config.INTERVAL} seconds for next loop")
        time.sleep(config.INTERVAL)


if __name__ == '__main__':
    main()