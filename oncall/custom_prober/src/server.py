import logging
import signal
import sys
import time

import typer
from prometheus_client import Gauge, Counter, start_http_server

from services import Router
from conf import ProberConfig as conf

CUSTOM_PROBER_CREATE_ROSTER_TOTAL = Counter(
    'custom_prober_create_roster_total',
    'Total count of roster create requests'
)
CUSTOM_PROBER_CREATE_ROSTER_SUCCESS_TOTAL = Counter(
    'custom_prober_create_roster_success_total',
    'Total count of successful roster create requests'
)
CUSTOM_PROBER_CREATE_ROSTER_FAIL_TOTAL = Counter(
    'custom_prober_create_roster_fail_total',
    'Total count of failed roster create requests'
)
CUSTOM_PROBER_CREATE_ROSTER_DURATION_SECONDS = Gauge(
    'custom_prober_create_roster_duration_seconds',
    'Total duration of roster create requests'
)


def probe(router) -> None:
    CUSTOM_PROBER_CREATE_ROSTER_TOTAL.inc()
    logging.info('Start creation of team')

    roster_name = "TestRoster42"
    team_name = "TestTeam42"

    start = time.perf_counter()
    create_resp = router.create_roster(team_name, roster_name)
    delete_resp = router.delete_roster(team_name, roster_name)
    duration = time.perf_counter() - start

    if create_resp and create_resp.status_code == 201 and delete_resp and delete_resp.status_code == 200:
        CUSTOM_PROBER_CREATE_ROSTER_SUCCESS_TOTAL.inc()
    else:
        CUSTOM_PROBER_CREATE_ROSTER_FAIL_TOTAL.inc()
    CUSTOM_PROBER_CREATE_ROSTER_DURATION_SECONDS.set(duration)


app = typer.Typer()


def setup_logging(config: conf):
    logging.basicConfig(
        stream=sys.stdout,
        level=config.LOG_LEVEL,
        format='%(asctime)s %(levelname)s:%(message)s'
    )


def terminate(signal, frame):
    print('Terminating...')
    sys.exit(0)


def probe_events():
    """
    Probe API method for roster creation and send data to Prometheus
    :interval - prove interval in seconds
    """
    signal.signal(signal.SIGTERM, terminate)
    config = conf()
    setup_logging(config)
    logging.info(f'Starting prober on port: {config.METRICS_PORT}')
    start_http_server(config.METRICS_PORT)
    router = Router(config.GOD_USER, config.DOMAIN, config.API_PATH)
    team = {
        "name": "TestTeam42",
        "scheduling_timezone": "Europe/Moscow",
        "email": "k8s@sre-course.ru",
        "slack_channel": "#k8s-team"
    }
    rsp = router.create_team(team)
    logging.info(f"Created test team - {rsp}")
    while True:
        logging.info('Run prober...')
        probe(router)
        logging.info(f'Waiting {config.INTERVAL} seconds for next probe')
        time.sleep(config.INTERVAL)


if __name__ == '__main__':
    typer.run(probe_events)
