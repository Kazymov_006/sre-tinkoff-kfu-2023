from environs import Env

env = Env()
env.read_env()


class ProberConfig:
    LOG_LEVEL = env('ONCALL_PROBER_LOG_LEVEL')
    METRICS_PORT = int(env('ONCALL_PROBER_METRICS_PORT'))
    DOMAIN = env('DOMAIN')
    API_PATH = env('API_PATH')
    INTERVAL = int(env('METRICS_INTERVAL'))
    GOD_USER = {"username": "root", "password": "123"}
