import time
import os

from prometheus_client import start_http_server, Gauge

from services import (get_linkedin_oncall_current_events_amount_gauge,
                      get_linkedin_oncall_teams_amount_gauge,
                      get_avg_response_time_microseconds_gauge)


DOMAIN = os.getenv('DOMAIN')
API_PATH = os.getenv('API_PATH')
INTERVAL = int(os.getenv('METRICS_INTERVAL'))


if __name__ == '__main__':
    api_url = DOMAIN + API_PATH

    teams_amount_gauge = Gauge('linkedin_oncall_teams_count', 'Количество активных команд в LinkedIn Oncall')
    events_amount_gauge = Gauge('linkedin_oncall_events_count', 'Количество событий в LinkedIn Oncall')
    avg_resp_time_microseconds_gauge = Gauge('linkedin_oncall_avg_response_time_microseconds', 'Среднее время ответа сервера на api-запрос')

    start_http_server(8001)
    while True:
        teams_amount = get_linkedin_oncall_teams_amount_gauge(api_url)
        if teams_amount is not None:
            teams_amount_gauge.set(teams_amount)
        events_amount = get_linkedin_oncall_current_events_amount_gauge(api_url)
        if events_amount is not None:
            events_amount_gauge.set(events_amount)
        avg_resp_time = get_avg_response_time_microseconds_gauge(api_url)
        if avg_resp_time is not None:
            avg_resp_time_microseconds_gauge.set(avg_resp_time)

        time.sleep(INTERVAL)
