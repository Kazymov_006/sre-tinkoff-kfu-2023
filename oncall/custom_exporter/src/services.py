from datetime import datetime

import requests

from requests.exceptions import ConnectionError


def get_linkedin_oncall_teams_amount_gauge(api_url: str) -> int | None:
    url = f'{api_url}/teams?active=1'
    try:
        teams_n = len(requests.get(url).json())
    except ConnectionError:
        teams_n = None
    return teams_n


def get_linkedin_oncall_current_events_amount_gauge(api_url: str) -> int | None:
    end_ge = datetime.now().timestamp()
    url = f'{api_url}/events?end__ge={end_ge}'
    try:
        events_n = len(requests.get(url).json())
    except ConnectionError:
        events_n = None
    return events_n


def get_single_request_response_time(url):
    try:
        response = requests.get(url)
        return response.elapsed.microseconds
    except ConnectionError:
        return


def get_avg_response_time_microseconds_gauge(api_url):
    endpoints = [
        '/teams',
        '/services',
        '/roles',
        '/events',
        '/users',
        '/notifications',
        '/audit',
    ]
    res = 0
    for endpoint in endpoints:
        resp = get_single_request_response_time(api_url+endpoint)
        if resp is None:
            return
        res += resp

    res /= len(endpoints)
    return res




