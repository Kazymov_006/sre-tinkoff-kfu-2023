set_only_oncall1_up(){
  docker exec nginx sh -c "cp /etc/nginx/tmp/extra_conf/oncall2down.conf /etc/nginx/conf.d/default.conf && usr/sbin/nginx -s reload"
}
set_only_oncall2_up(){
  docker exec nginx sh -c "cp /etc/nginx/tmp/extra_conf/oncall1down.conf /etc/nginx/conf.d/default.conf && usr/sbin/nginx -s reload"
}
set_both_oncall_up(){
  docker exec nginx sh -c "cp /etc/nginx/tmp/extra_conf/oncall_default.conf /etc/nginx/conf.d/default.conf && usr/sbin/nginx -s reload"
}

zero_downtime_update() {
  echo "Shutting down second instance of oncall"
  set_only_oncall1_up
  sleep 15

  docker stop oncall_oncall-web_2
  docker rm oncall_oncall-web_2

  echo "Updating second instance of oncall"
  docker compose up -d --build --no-deps oncall-web-2

  echo "Starting second instance to allow connections"
  set_both_oncall_up
  sleep 15

  echo "Shutting down first instance of oncall"
  set_only_oncall2_up
  sleep 15

  docker stop oncall_oncall-web_1
  docker rm oncall_oncall-web_1

  echo "Updating first instance of oncall"
  docker compose up -d --build --no-deps oncall-web-1

  echo "Starting first instance to allow connections"
  set_both_oncall_up

  echo "Oncall update finished."
}

zero_downtime_update
