import datetime
import typing
import logging

import requests
import yaml

import conf


class LoginCreds(typing.TypedDict):
    username: str
    password: str


class Team(typing.TypedDict):
    name: str
    scheduling_timezone: str
    email: str
    slack_channel: str


class User(typing.TypedDict):
    name: str
    full_name: str
    phone_number: str
    email: str


class Router:
    def __init__(self, god_user: LoginCreds, domain: str, api_path: str) -> None:
        self.god_user = god_user
        self.csrf_token = None
        self.cookie = None
        self.domain = domain
        self.full_api_path = self.domain + api_path
        self.renew_csrf_and_cookie()

    def get_headers(self) -> dict:
        return {
            "X-Csrf-Token": self.csrf_token,
            "Cookie": self.cookie
        }

    def renew_csrf_and_cookie(self) -> None:
        resp = requests.post(f"{self.domain}/login", data=self.god_user)
        if resp.json().get("csrf_token") is not None:
            self.csrf_token = resp.json().get("csrf_token")
        if resp.headers.get('Set-cookie') is not None:
            self.cookie = resp.headers.get('Set-cookie')

    def create_team(self, team: Team) -> requests.Response:
        if self.cookie is None or self.csrf_token is None:
            self.renew_csrf_and_cookie()
        resp = requests.post(f"{self.full_api_path}/teams", headers=self.get_headers(), json=team)
        return resp

    def create_user(self, user: User) -> requests.Response:
        if self.cookie is None or self.csrf_token is None:
            self.renew_csrf_and_cookie()
        resp = requests.post(
            f"{self.full_api_path}/users",
            headers=self.get_headers(),
            json={"name": user.get("name")}
        )
        if resp.status_code == 422:
            return resp
        data = {
            "full_name": user.get("full_name"),
            "contacts": {
                "call": user.get("phone_number"),
                "email": user.get("email"),
                "sms": user.get("phone_number")
            }
        }
        resp = requests.put(f"{self.full_api_path}/users/{user.get('name')}", headers=self.get_headers(), json=data)
        return resp

    def create_roster(self, team_name: str, roster_name: str) -> requests.Response:
        resp = requests.post(f"{self.full_api_path}/teams/{team_name}/rosters", json={"name": roster_name})
        return resp

    def add_user_to_roster(self, team_name: str, roster_name: str, user_name: str) -> requests.Response:
        if self.cookie is None or self.csrf_token is None:
            self.renew_csrf_and_cookie()
        resp = requests.post(
            f"{self.full_api_path}/teams/{team_name}/rosters/{roster_name}/users",
            headers=self.get_headers(),
            json={"name": user_name}
        )
        return resp

    def create_event(self, datestr: str, role: str, team_name: str, user_name: str) -> requests.Response:
        time_start = datetime.datetime.strptime(datestr, "%d/%m/%Y")
        time_end = time_start + datetime.timedelta(hours=23, minutes=59, seconds=59)
        data = {
            "start": int(time_start.timestamp()),
            "end": int(time_end.timestamp()),
            "user": user_name,
            "team": team_name,
            "role": role
        }
        resp = requests.post(f"{self.full_api_path}/events", headers=self.get_headers(), json=data)
        return resp


def main() -> None:
    logging.basicConfig(level=logging.INFO, filename=conf.LOG_FILE_PATH, filemode="w",
                        format="%(asctime)s %(levelname)s %(message)s")
    router = Router(conf.GOD_USER, conf.DOMAIN, conf.API_PATH)

    with open(conf.YAML_FILE_PATH, "r") as stream:
        try:
            data_from_yaml = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            logging.critical(exc)
            raise exc

    for team in data_from_yaml["teams"]:
        data = dict(filter(lambda key_value: key_value[0] != "users", team.items()))
        resp = router.create_team(data)
        logging.info(f"Team \"{team['name']}\" creation response: status code - {resp.status_code}, {resp.text}")
        resp = router.create_roster(team["name"], "roster1")
        logging.info(f"Roster \"roster1\" creation response: status code - {resp.status_code}, {resp.text}")
        for user in team["users"]:
            data = dict(filter(lambda key_value: key_value[0] != "duty", user.items()))
            resp = router.create_user(data)
            logging.info(f"User \"{user['name']}\" creation response: status code - {resp.status_code}, {resp.text}")
            resp = router.add_user_to_roster(team["name"], "roster1", user["name"])
            logging.info(f"User \"{user['name']}\" addition to roster \"roster1\" of team \"{team['name']}\" response: "
                         f"status code - {resp.status_code}, {resp.text}")
            for duty in user["duty"]:
                resp = router.create_event(duty["date"], duty["role"], team["name"], user["name"])
                logging.info(f"Event creation for user \"{user['name']}\" on {duty['date']} response: "
                             f"status code - {resp.status_code}, {resp.text}")


if __name__ == '__main__':
    main()
